from typing import Any

import requests


class VkException(Exception):
    pass


class VkRequest:
    def __init__(self, access_token: str, version: float):
        self._access_token = access_token
        self._version = version

    def __call__(self, method: str, **kwargs) -> Any:
        api_url = 'https://api.vk.com/method/'
        retry_errors = {
            6: 'Слишком много запросов в секунду',
            9: 'Слишком много однотипных действий'
        }
        stop_errors = {
            1: 'Произошла неизвестная для ВК ошибка.',
            5: 'Авторизация не удалась. Нужно получить новый access token',
            10: 'Произошла внутренняя ошибка сервера ВК.',
            29: 'Достигнут количественный лимит на вызов метода',
            203: 'Доступ к группе запрещён',
            224: 'Слишком много рекламных постов.',
            600: 'Нет прав на выполнение данных операций с рекламным кабинетом',
            601: 'Отказано в доступе. Вы превысили ограничение на количество запросов за день. Попробуйте позже.',
            603: 'Произошла ошибка при работе с рекламным кабинетом'
        }
        try:
            response = requests.post(api_url + method, data={**kwargs,
                                                             'access_token': self._access_token,
                                                             'v': self._version
                                                             })
        except requests.RequestException as e:
            raise VkException('Ошибка связи: ' + str(e))
        if not response.status_code == requests.codes.ok:
            raise VkException('Ошибка ответа')
        json_response = response.json()
        if 'error' in json_response:
            error_code = json_response['error']['error_code']
            if error_code in retry_errors:
                return self.__call__(method, **kwargs)
            elif error_code in stop_errors:
                raise VkException(stop_errors[error_code])
            else:
                raise VkException(f"Возникла неизвестная ошибка: {json_response['error']['error_msg']}, "
                                  f"params: {kwargs}")
        else:
            return json_response['response']
