import logging
import re
from datetime import datetime, timezone, timedelta
from typing import Any, Dict, List, Union

from vk_api import VkRequest


class Post:
    def __init__(self, owner_id: int, post_id: int, timestamp: int, text: str, is_pinned: Union[bool, None]):
        self.owner_id = owner_id
        self.post_id = post_id
        self.datetime = datetime.fromtimestamp(timestamp, timezone(timedelta(hours=3)))
        self.text = text
        self.is_pinned = bool(is_pinned)

    def __repr__(self) -> str:
        return f'https://vk.com/wall{self.owner_id}_{self.post_id}'

    @property
    def _was_published_recently(self) -> bool:
        now = datetime.now(timezone(timedelta(hours=3)))
        return now - self.datetime < timedelta(days=2) and self.datetime <= now

    @property
    def _contains_web_link(self) -> bool:
        """Checks if post's text contains any link except new article format"""
        regex = '(?:http://www\.|https://www\.|http://|https://|www\.)[A-я]+\.[A-я]+[/@-_-&*$#!%^()A-я0-9]*'
        matches = re.findall(regex, self.text)
        for match in matches:
            if 'vk.com/@' not in match:
                return True
        return False

    @property
    def _contains_vk_link(self) -> bool:
        return bool(re.search(r'\[club\d+\|.+\]', self.text))

    @property
    def for_delete(self) -> bool:
        return ((not self.is_pinned) and
                (self._contains_web_link or self._contains_vk_link) and
                self._was_published_recently)


class PublicList(list):
    def __contains__(self, o: str) -> bool:
        for item in self:
            if item.casefold() in o.casefold():
                return True
        return False


class GroupNotFound(Exception):
    pass


class Api(VkRequest):
    def __init__(self, access_token: str, version: float):
        super().__init__(access_token, version)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

    def __call__(self, *args, **kwargs):
        pass

    def raw_request(self, method: str, **kwargs) -> Any:
        return super().__call__(method, **kwargs)

    def _get_public_ids(self) -> List[int]:
        offset = 0
        publics = []
        step = 1000
        while True:
            response = super().__call__('groups.get', filter='moder', extended=1, count=step,
                                        offset=offset)
            publics.extend(response['items'])
            if response['count'] - offset <= step:
                break
            offset += step
        public_ids = []
        for public in publics:
            public_ids.append(public['id'])
        return public_ids

    def merge_user_and_file_publics(self, file_publics: List[str]) -> List[int]:
        public_ids = []
        user_publics = self._get_public_ids()
        for public_id in file_publics:
            public_id = int(public_id)
            if public_id in user_publics:
                public_ids.append(public_id)
        return public_ids

    def get_public_posts(self, public_id: int) -> List[Post]:
        wall = super().__call__('wall.get', owner_id='-' + str(public_id), count=100, filter='owner')
        if not wall:
            return []
        posts = []
        for item in wall['items']:
            posts.append(Post(item['owner_id'], item['id'], item['date'], item['text'], item.get('is_pinned')))
        return posts

    def get_posts_by_id(self, post_list: List[str]) -> List[Post]:
        vk_posts = []
        offset = 0
        while True:
            str_list = ''
            for post in post_list[offset:]:
                str_list += post + ','
            str_list = str_list[:-1]
            response = super().__call__('wall.getById', posts=str_list)
            if response and len(response) > 0:
                vk_posts.extend(response)
            if len(post_list[offset:]) <= offset + 100:
                break
            offset += 100
        post_objects = []
        for item in vk_posts:
            if item['post_type'] not in ['postpone', 'suggest']:
                post_objects.append(Post(item['owner_id'], item['id'],
                                         item['date'], item['text'], item.get('is_pinned')))
        return post_objects

    def delete_post(self, post: Post):
        return super().__call__('wall.delete', owner_id=post.owner_id, post_id=post.post_id)

    def convert_group_id_to_name(self, group_id: int) -> Union[str, int]:
        response = super().__call__('groups.getById', group_id=abs(group_id))
        if not (response and isinstance(response, list) and isinstance(response[0], dict)):
            self.logger.debug("Could not convert id %s to name", group_id)
            try:
                return self._static_convert_group_id_to_name(group_id)
            except GroupNotFound:
                return group_id
        return response[0]['name']

    def _static_convert_group_id_to_name(self, group_id: int) -> str:
        publics = {
            -157382690: 'Совершенство жизни',
            -145894559: '# Все в Твоих Руках',
            -146318366: '# Сила Внутри Тебя',
            -75767353: 'Дом восходящего солнца',
            -145784366: '☆ ЭЗОТЕРИКА ☆'
        }
        try:
            return publics[group_id]
        except KeyError:
            self.logger.debug('Group %s not found in static dict', group_id)
            raise GroupNotFound
