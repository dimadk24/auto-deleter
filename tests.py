import os
import time
import unittest
from datetime import time as dttime, timezone, timedelta, datetime
from unittest import TestCase, skip

from main import get_data_from_file, strip_list_items, check_time, BadTimeFormat, parse_time, parse_timedelta, \
    datetime_in_time_list
from vk_highlevel_api import Post, PublicList


class GetDataFromFileTest(TestCase):
    filename = 'test.tmp'

    def tearDown(self):
        if os.path.isfile(self.filename):
            os.remove(self.filename)

    def test_simple_file(self):
        file_content = 'Тестовая страница\nДрема\nMoney Time'
        with open(self.filename, 'x', encoding='utf8') as f:
            f.write(file_content)
        result = get_data_from_file(self.filename)
        true_result = ['Тестовая страница', 'Дрема', 'Money Time']
        self.assertListEqual(result, true_result)

    def test_raises(self):
        with self.assertRaises(OSError):
            get_data_from_file(self.filename)

    def test_stripes(self):
        file_content = '   Тестовая страница  \n   Дрема  \n  Money Time'
        with open(self.filename, 'x', encoding='utf8') as f:
            f.write(file_content)
        result = get_data_from_file(self.filename)
        true_result = ['Тестовая страница', 'Дрема', 'Money Time']
        self.assertListEqual(result, true_result)

    def test_stripes_new_lines_and_none_lines(self):
        file_content = '\n   Тестовая страница  \n \n  Дрема \n \n  Money Time\n   \n   '
        with open(self.filename, 'x', encoding='utf8') as f:
            f.write(file_content)
        result = get_data_from_file(self.filename)
        true_result = ['Тестовая страница', 'Дрема', 'Money Time']
        self.assertListEqual(result, true_result)


class StripListItemsTest(TestCase):
    def test_stripes(self):
        test_list = ['   test   ', '  aloha  ']
        result_list = ['test', 'aloha']
        self.assertListEqual(strip_list_items(test_list), result_list)

    def test_dont_stripes(self):
        test_list = ['test', 'aloha']
        self.assertListEqual(strip_list_items(test_list), test_list)


class CheckTimeTest(TestCase):
    def test_simple(self):
        time_str = '22:30'
        self.assertEqual(check_time(time_str), time_str)

    def test_one_digit_hours(self):
        time_str = '1:30'
        result_time = '01:30'
        self.assertEqual(check_time(time_str), result_time)

    def test_one_digit_minutes(self):
        time_str = '01:3'
        result_time = '01:03'
        self.assertEqual(check_time(time_str), result_time)

    def test_one_digit_both(self):
        time_str = '1:3'
        result_time = '1:03'
        self.assertEqual(check_time(time_str), result_time)

    def test_raises(self):
        time_str = '041:32'
        with self.assertRaises(BadTimeFormat):
            check_time(time_str)


class ParseTimeTest(TestCase):
    minsk_tz = timezone(timedelta(hours=3))
    time_str = '21:30, 10:35, 01:07'

    def test_simple(self):
        result_time_objs = [dttime(21, 30, tzinfo=self.minsk_tz),
                            dttime(10, 35, tzinfo=self.minsk_tz),
                            dttime(1, 7, tzinfo=self.minsk_tz)]
        self.assertEqual(parse_time(self.time_str), result_time_objs)

    def test_is_instanse(self):
        for item in parse_time(self.time_str):
            self.assertIsInstance(item, dttime)

    def test_empty_line(self):
        time_str = ''
        self.assertEqual(parse_time(time_str), [])

    def test_raises_in_checker(self):
        bad_str_time = '25:61, 070:72'
        with self.assertRaises(BadTimeFormat):
            parse_time(bad_str_time)

    def test_raises(self):
        bad_str_time = '61:85, 12:30'
        with self.assertRaises(BadTimeFormat):
            parse_time(bad_str_time)


class ParseTimeDeltaTest(TestCase):
    def test_simple(self):
        input_str = '08:30'
        result = timedelta(hours=8, minutes=30)
        self.assertEqual(parse_timedelta(input_str), result)

    def test_raises(self):
        input_str = '25:10'
        with self.assertRaises(BadTimeFormat):
            parse_timedelta(input_str)


class DatetimeInTimeListTest(TestCase):
    minsk_tz = timezone(timedelta(hours=3))

    def test_with_minks_tz(self):
        time_list = [dttime(hour=10, minute=15, tzinfo=self.minsk_tz),
                     dttime(hour=22, minute=30, tzinfo=self.minsk_tz)]
        datetime_list = [datetime(year=2018, month=6, day=2, hour=10, minute=15, tzinfo=self.minsk_tz),
                         datetime(year=2018, month=6, day=2, hour=22, minute=30, tzinfo=self.minsk_tz)]
        for datetime_obj in datetime_list:
            self.assertTrue(datetime_in_time_list(time_list, datetime_obj))

    def test_with_utc_tz(self):
        time_list = [dttime(hour=10, minute=15, tzinfo=timezone.utc),
                     dttime(hour=22, minute=30, tzinfo=timezone.utc)]
        datetime_list = [datetime(year=2018, month=6, day=2, hour=10, minute=15, tzinfo=timezone.utc),
                         datetime(year=2018, month=6, day=2, hour=22, minute=30, tzinfo=timezone.utc)]
        for datetime_obj in datetime_list:
            self.assertTrue(datetime_in_time_list(time_list, datetime_obj))

    def test_with_naive(self):
        time_list = [dttime(hour=10, minute=15),
                     dttime(hour=22, minute=30)]
        datetime_list = [datetime(year=2018, month=6, day=2, hour=10, minute=15),
                         datetime(year=2018, month=6, day=2, hour=22, minute=30)]
        for datetime_obj in datetime_list:
            self.assertTrue(datetime_in_time_list(time_list, datetime_obj))

    @skip('Feature to work with different tzs isn\'t supported yet')
    def test_with_different_tzs(self):
        time_list = [dttime(hour=10, minute=15, tzinfo=self.minsk_tz),
                     dttime(hour=22, minute=30, tzinfo=self.minsk_tz)]
        datetime_list = [datetime(year=2018, month=6, day=2, hour=7, minute=15, tzinfo=timezone.utc),
                         datetime(year=2018, month=6, day=2, hour=19, minute=30, tzinfo=timezone.utc)]
        for datetime_obj in datetime_list:
            self.assertTrue(datetime_in_time_list(time_list, datetime_obj))

    def test_false(self):
        time_list = [dttime(hour=11, minute=15),
                     dttime(hour=21, minute=30)]
        datetime_list = [datetime(year=2018, month=6, day=2, hour=10, minute=15),
                         datetime(year=2018, month=6, day=2, hour=22, minute=30)]
        for datetime_obj in datetime_list:
            self.assertFalse(datetime_in_time_list(time_list, datetime_obj))


class PostTest(TestCase):
    minsk_tz = timezone(timedelta(hours=3))

    def test_simple_positive_owner_id(self):
        post = Post(1, 1, int(time.time() - timedelta(hours=3).total_seconds()),
                    'sldjflkdslgjf', False)
        self.assertEqual(post.datetime, datetime.now(tz=self.minsk_tz).replace(microsecond=0) - timedelta(hours=3))
        self.assertFalse(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_simple_negative_owner_it(self):
        post = Post(-1, 1, int(time.time() - timedelta(hours=3).total_seconds()),
                    'sldjflkdslgjf', False)
        self.assertEqual(post.datetime, datetime.now(tz=self.minsk_tz).replace(microsecond=0) - timedelta(hours=3))
        self.assertFalse(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_datetime(self):
        post = Post(1, 1, int(time.time() + timedelta(hours=3).total_seconds()),
                    'sldjflkdslgjf', False)
        self.assertEqual(post.datetime, datetime.now(tz=self.minsk_tz).replace(microsecond=0) + timedelta(hours=3))
        self.assertFalse(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_contains_web_link(self):
        post = Post(1, 1, int(time.time()), 'yeye https://google.com great code', False)
        self.assertTrue(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_misk_tz(self):
        # time.time gets utc timestamp, Post converts it into aware datetime with tz=utc+3
        # so we have to check it with minsk tz
        post = Post(1, 1, int(time.time()), 'yeye cool code', False)
        self.assertEqual(post.datetime, datetime.now(tz=self.minsk_tz).replace(microsecond=0))

    def test_is_pinned(self):
        post = Post(1, 1, int(time.time()), 'yeye https://google.com cool code', True)
        self.assertFalse(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_vk_link(self):
        post = Post(1, 1, int(time.time()), 'yeye [club1|apiclub] cool code', False)
        self.assertTrue(post.for_delete)
        self.assertTrue(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_both_links(self):
        post = Post(1, 1, int(time.time()), 'yeye [club1|apiclub] cool code https://yandex.ru/?true=true',
                    False)
        self.assertTrue(post.for_delete)
        self.assertTrue(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_http_web_link(self):
        post = Post(1, 1, int(time.time()), 'yeye http://google.com cool code', False)
        self.assertTrue(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_vk_article_link(self):
        post = Post(1, 1, int(time.time()), 'yeye https://vk.com/@moneymonkey-marketing-fishechki cool', False)
        self.assertFalse(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_vk_article_and_vk_link(self):
        post = Post(1, 1, int(time.time()), 'yeye https://vk.com/@moneymonkey-marketing-fishechki cool'
                                            'dlfjgldfkg[club1|apiclub]sldfkjalskdjf', False)
        self.assertTrue(post.for_delete)
        self.assertTrue(post._contains_vk_link)
        self.assertFalse(post._contains_web_link)

    def test_vk_article_and_web_link(self):
        post = Post(1, 1, int(time.time()), 'yeye https://vk.com/@moneymonkey-marketing-fishechki cool'
                                            'https://google.com sldfjl', False)
        self.assertTrue(post.for_delete)
        self.assertFalse(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_vk_article_and_web_link_and_vk_link(self):
        post = Post(1, 1, int(time.time()), 'yeye https://vk.com/@moneymonkey-marketing-fishechki cool'
                                            'https://google.com sldfjl [club58694563|lsdkafsalk@]', False)
        self.assertTrue(post.for_delete)
        self.assertTrue(post._contains_vk_link)
        self.assertTrue(post._contains_web_link)

    def test_simple_today(self):
        post = Post(1, 1, int(time.time()), 'cool text', False)
        self.assertTrue(post._was_published_recently)

    def test_recently_47_hours_ago(self):
        post = Post(1, 1, int((datetime.now() - timedelta(hours=47)).timestamp()), 'tx', False)
        self.assertTrue(post._was_published_recently)

    def test_recently_almost_2_days_ago(self):
        post = Post(1, 1, int((datetime.now() - timedelta(hours=47, minutes=59)).timestamp()), 'tx', False)
        self.assertTrue(post._was_published_recently)

    def test_not_recently_simple(self):
        post = Post(1, 1, int((datetime.now() - timedelta(hours=48, seconds=1)).timestamp()), 'tx', False)
        self.assertFalse(post._was_published_recently)

    def test_not_recently_tommorow(self):
        now = datetime.now()
        post = Post(1, 1, int((now.replace(day=now.day + 1)).timestamp()), 'tx', False)
        self.assertFalse(post._was_published_recently)

    def test_not_recently_soon(self):
        post = Post(1, 1, int((datetime.now() + timedelta(seconds=1)).timestamp()), 'tx', False)
        self.assertFalse(post._was_published_recently)


class PublicListTest(TestCase):
    def test_in(self):
        public_list = PublicList(['страница', 'Time'])
        self.assertTrue('Тестовая страница' in public_list)
        self.assertTrue('Money Time' in public_list)

    def test_equals(self):
        public_list = PublicList(['Тестовая страница', 'Money Time'])
        self.assertTrue('Тестовая страница' in public_list)
        self.assertTrue('Money Time' in public_list)

    def test_casefold(self):
        public_list = PublicList(['тестовая Страница', 'money time'])
        self.assertTrue('Тестовая страница' in public_list)
        self.assertTrue('Money Time' in public_list)

    def test_out(self):
        public_list = PublicList(['страница', 'Time'])
        self.assertFalse('странца' in public_list)
        self.assertFalse('Тестоваяа' in public_list)
        self.assertFalse('Monkey' in public_list)
        self.assertTrue('Тестовая страница ' in public_list)
        self.assertTrue(' Money Time' in public_list)


if __name__ == '__main__':
    unittest.main()
