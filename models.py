import os

from dotenv import load_dotenv
from peewee import *
from playhouse.db_url import connect

load_dotenv(os.path.join(os.path.dirname(os.path.abspath(__file__)), '.env'), verbose=True)
db = connect(os.environ.get('DATABASE_URL'))


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    date_joined = DateTimeField()
    email = CharField()
    first_name = CharField()
    is_active = BooleanField()
    is_staff = BooleanField()
    is_superuser = BooleanField()
    last_login = DateTimeField(null=True)
    last_name = CharField()
    password = CharField()
    username = CharField(unique=True)

    class Meta:
        table_name = 'auth_user'


class Type(BaseModel):
    text = CharField()

    class Meta:
        table_name = 'script_type'


class Post(BaseModel):
    created = DateTimeField()
    datetime = DateTimeField()
    image = CharField()
    link = CharField()
    new_post_id = CharField(column_name='new_post_id')
    public = CharField()
    text = TextField()
    type = ForeignKeyField(column_name='type_id', field='id', model=Type, null=True)
    user = ForeignKeyField(column_name='user_id', field='id', model=User)

    class Meta:
        table_name = 'script_post'
