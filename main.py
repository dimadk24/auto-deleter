import io
import logging
import os
import re
from datetime import time, datetime, timezone, timedelta
from time import sleep
from typing import List, Union

from dotenv import load_dotenv as dotenv_load_dotenv, find_dotenv
from raven.handlers.logging import SentryHandler
from telegram import Bot
from telegram.error import TimedOut
from telegram.utils.request import Request

from models import Post as DBPost, Type, User
from vk_highlevel_api import Api, PublicList


class BadTimeFormat(Exception):
    pass


def get_data_from_file(filename: str, stripparts: bool = True, sep: str = '\n') -> List[str]:
    lines = []
    try:
        with open(filename, encoding='utf8') as f:
            file_content = f.read()
            if file_content.endswith(sep.strip()):
                file_content = file_content[: file_content.rfind(sep.strip())]
            for part in file_content.split(sep):
                if stripparts:
                    part = part.strip()
                if not part:
                    continue
                lines.append(part)
    except OSError:
        raise OSError(f'File not found: {filename}')
    return lines


def unicode_load_dotenv(filename):
    dotenv = find_dotenv(filename)
    with open(dotenv, encoding='utf-8') as f:
        stream = io.StringIO(f.read())
    return dotenv_load_dotenv(stream, override=True)


def strip_list_items(input_list: List[str]) -> List[str]:
    new_list = []
    for item in input_list:
        new_list.append(item.strip())
    return new_list


def check_time(time_str: str) -> str:
    regex = re.compile('\d{1,2}:\d{1,2}')
    if not regex.fullmatch(time_str):
        raise BadTimeFormat(f'Неверный формат времени "{time_str}". Нужно: "ЧЧ:ММ" или "Ч:ММ" или "Ч:М"')
    if not len(time_str) == 5:
        partition_tuple = time_str.partition(':')
        if len(partition_tuple[0]) == 1:
            time_str = '0' + time_str
        if len(partition_tuple[2]) == 1:
            time_str = partition_tuple[0] + partition_tuple[1] + '0' + partition_tuple[2]
    return time_str


def parse_time(string: str) -> Union[List[time], List]:
    if not string:
        return []
    str_time_list = strip_list_items(string.split(','))
    time_list = []
    for str_time in str_time_list:
        time_list.append(check_time(str_time))
    time_objects = []
    for time_str in time_list:
        try:
            datetime_obj = datetime.strptime(time_str, '%H:%M').replace(tzinfo=timezone(timedelta(hours=3)))
            time_obj = time(datetime_obj.hour, datetime_obj.minute, tzinfo=datetime_obj.tzinfo)
        except ValueError as e:
            raise BadTimeFormat(f'strptime raised exception. Скорее всего неверное время "{time_str}": ' + str(e.args))
        time_objects.append(time_obj)
    return time_objects


def parse_timedelta(string: str) -> timedelta:
    try:
        datetime_obj = datetime.strptime(check_time(string), '%H:%M')
    except ValueError as e:
        raise BadTimeFormat(f'strptime raised exception. Скорее всего неверное время "{string}": ' + str(e.args))
    return timedelta(hours=datetime_obj.hour, minutes=datetime_obj.minute)


def datetime_in_time_list(time_list: List[time], datetime: datetime) -> bool:
    # Works only if all items of time list and datetime are all in one tz or all are naive
    for time_obj in time_list:
        if time_obj.hour == datetime.hour and time_obj.minute == datetime.minute:
            return True
    return False


def get_posts_from_db(user) -> List[str]:
    db_posts = DBPost.select().where(
        (DBPost.type == Type.get(Type.text == 'Done')) &
        (DBPost.user == User.get(User.username == user)) &
        (DBPost.datetime.between(datetime.utcnow() - timedelta(days=1), datetime.utcnow())))
    str_posts = []
    for db_post in db_posts:
        str_posts.append(db_post.new_post_id)
    return str_posts


class BadDataGiven(Exception):
    pass


def get_text_title(text: str) -> str:
    return text.splitlines()[0]


class TgNotifier:
    def __init__(self, bot_token: str, chat_id: int,
                 success_message_format: str, error_message_format: str):
        self.bot = Bot(bot_token, request=Request(connect_timeout=15, read_timeout=15))
        self.chat_id = chat_id
        self.success_message_format = success_message_format
        self.error_message_format = error_message_format

    def notify(self, public: str, post_text: str, success: bool):
        message = self._create_message_text(success, public, post_text)
        self._send(message)

    def _create_message_text(self, success: bool, public: str, post_text: str) -> str:
        message_format = ('success' if success else 'error') + '_message_format'
        return getattr(self, message_format).format(public=public,
                                                    post_text=get_text_title(post_text))

    def _send(self, message: str):
        i = 0
        while True:
            try:
                self.bot.sendMessage(self.chat_id, message, parse_mode='Markdown')
                break
            except TimedOut:
                sleep(20)
                i += 1
                if i > 2:
                    raise


def run():
    os.environ['AUTODELETER_USER'] = 'Дима'
    success_text_format = '✅ Пост _{post_text}_ успешно удален из паблика _{public}_'
    error_text_format = '😔 При удалении поста _{post_text}_ из паблика _{public}_ возникла ошибка'
    logger = logging.getLogger(__name__)
    logger.info('Script started')
    settings_file = os.environ.get('SETTINGS_FILE')
    if not (os.path.exists(settings_file) and os.path.isfile(settings_file)):
        raise BadDataGiven('settings_file doesn\'t exists')
    publics_file = os.environ.get('PUBLICS_FILE')
    if not (os.path.exists(publics_file) and os.path.isfile(publics_file)):
        raise BadDataGiven('publics_file doesn\'t exists')
    unicode_load_dotenv(settings_file)
    access_token = os.environ.get('access_token').strip()
    exclude_time = os.environ.get('exclude_time').strip()
    ttl = os.environ.get('time_to_live').strip()
    user = os.environ.get('AUTODELETER_USER').strip()
    if not (access_token and exclude_time and ttl and user):
        logger.error('Not all params present in settings.ini and .env')
        if not access_token:
            logger.error('no access_token')
        if not exclude_time:
            logger.error('no exclude_time')
        if not ttl:
            logger.error('no ttl')
        if not user:
            logger.critical('no USER in .env')
        raise BadDataGiven()
    exclude_time = parse_time(exclude_time)
    ttl = parse_timedelta(ttl)

    r = Api(access_token, 5.95)
    sender = TgNotifier(
        os.environ.get('TG_BOT_TOKEN'),
        os.environ.get('TG_CHAT_ID'),
        success_text_format,
        error_text_format,
    )
    publics = get_data_from_file(os.environ.get('PUBLICS_FILE'))
    logger.debug('Got %s public ids from file: %s', len(publics), publics)
    publics = r.merge_user_and_file_publics(publics)
    logger.info('Got %s public ids from file and vk (with nedded rules): %s', len(publics), publics)
    deleted = 0
    tz = timezone(timedelta(hours=3))
    day_timedelta = timedelta(hours=24)
    for public in publics:
        public_deleted = 0
        public_posts = r.get_public_posts(public)
        logger.debug('Loaded %s last posts from public with id %s', len(public_posts), public)
        for post in public_posts:
            if (post.for_delete and (
                        (
                                    (not datetime_in_time_list(exclude_time, post.datetime)) and
                                    (datetime.now(tz) - post.datetime >= ttl)
                        ) or
                        (
                                    datetime_in_time_list(exclude_time, post.datetime) and
                                    (datetime.now(tz) - post.datetime >= day_timedelta)
                        ))):
                logger.debug('Deleting post %s', post)
                response = r.delete_post(post)
                if response == 1:
                    logger.debug('Deleted post %s from public %s', str(post), public)
                    public_deleted += 1
                    sender.notify(r.convert_group_id_to_name(post.owner_id), post.text, True)
                else:
                    logger.debug('Post %s should be deleted but error happened. Here\'s the vk '
                             'response: %s',
                                 str(post), response, extra={'vk_response': response})
                    sender.notify(r.convert_group_id_to_name(post.owner_id), post.text, False)
        logger.info('Deleted %s posts from public %s', public_deleted, public)
        deleted += public_deleted
    logger.info('Deleted %s posts from all publics', deleted)

    clicker_posts = get_posts_from_db(user)
    clicker_deleted = 0
    if len(clicker_posts) == 0:
        logger.debug('No posts in clicker database with given params')
    else:
        logger.debug('Got %s posts from clicker database: %s', len(clicker_posts), clicker_posts)
        clicker_posts = r.get_posts_by_id(clicker_posts)
        logger.info('Got %s clicker posts from vk: %s', len(clicker_posts), clicker_posts)
        for post in clicker_posts:
            if abs(post.owner_id) not in publics:
                continue
            if ((not post.is_pinned) and (
                        (
                                    (not datetime_in_time_list(exclude_time, post.datetime)) and
                                    (datetime.now(tz) - post.datetime >= ttl)
                        ) or
                        (
                                    datetime_in_time_list(exclude_time, post.datetime) and
                                    (datetime.now(tz) - post.datetime >= day_timedelta)
                        ))):
                logger.debug('Deleting post %s', post)
                response = r.delete_post(post)
                if response == 1:
                    logger.debug('Deleted post %s', str(post))
                    clicker_deleted += 1
                    sender.notify(r.convert_group_id_to_name(post.owner_id), post.text, True)
                else:
                    logger.error('Post %s should be deleted but error happened. Here\'s the vk response: %s',
                                 str(post), response, extra={'vk_response': response})
                    sender.notify(r.convert_group_id_to_name(post.owner_id), post.text, False)
        logger.info('Deleted %s posts from clicker database', clicker_deleted)
    logger.info('OVERALL DELETED %s POSTS', str(deleted + clicker_deleted))


if __name__ == '__main__':
    unicode_load_dotenv('.env')
    now_obj = datetime.now(timezone(timedelta(hours=3)))
    month, day = str(now_obj.month), str(now_obj.day)
    logging_dir = os.path.join(os.environ.get('LOGGING_PATH'), 'AutoDeleter', month, day)
    if not os.path.exists(logging_dir):
        os.makedirs(logging_dir)
    logging_file = os.path.join(logging_dir, 'logging.log')
    logging_level = os.environ.get('LOGGING_LEVEL', 'INFO')
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(logging_file)
    file_handler.setLevel(getattr(logging, logging_level.upper(), logging.INFO))
    formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s: %(message)s', datefmt='%d.%m.%Y %H:%M')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    if not int(os.environ.get('DEBUG', 0)):
        sentry_handler = SentryHandler(os.environ.get('SENTRY_DSN'))
        sentry_handler.setLevel(logging.ERROR)
        logger.addHandler(sentry_handler)
    try:
        run()
    except Exception:
        logger.exception('Exception raised by run', exc_info=True)
